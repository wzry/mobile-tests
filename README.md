# README #

### What is this repository for? ###

* This repository contains program codes for the Onvocal Mobile testing. 
* The solution currently only contain automation test for the location feature of the SDK. 

### What is this repository contain? ###

* This repository contains 2 projects. The Mobile.Test.Framework and Mobile.Test.Suite for the Onvocal Mobile testing. 
* The repository also contain an Apple script folder and an Android test app folder.
* The Apple script folder has all osascipts to automate actions on the IOS simulator from the macbook. 
* These osascripts are place in the desktop folder of wayne's macbook and can excuted by the framework using ssh connections.
* The Android test app folder contain a test service created on the android sdk test app to simulate mock activityResult for the google activity Recognition serivces.
* The service is called by the framework using cmd command line of with adb command tools.

### How to run tests? ###

```


* To run the automated test, you need to open the project in visual studio 2017 and run the test on Visual Studio Test Explorer.

* Also the test framework is using appium to connected to mobile devices undertest. 
* The current android appium server is pointing to Wayne's Window PC and IOS appium server is pointing to Wayne's Macbook. 
* This connection needed to be open while the test is running.


```

### Questions? ###

* Please speak with Wayne Yi if you have additional questions regarding this repository.