package com.roamingaround.sdk.app;

/**
 * Created by Wayne Yi on 5/4/17.
 */

import android.app.IntentService;
import android.content.Intent;


import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.roamingaround.sdk.LogUtil;

import java.util.List;
import java.util.ArrayList;
import android.os.SystemClock;


public class ActivityMockTestService extends IntentService {

    private final String TAG = getClass().getSimpleName();

    public ActivityMockTestService() {
        super("ActivityMockTestService");
    }
/*

        @Override
        public void onCreate() {
            super.onCreate();

            LogUtil.v(TAG, "service onCreate");
        }

        @Override
        public void onDestroy() {
            super.onDestroy();

            LogUtil.v(TAG, "service onDestroy");
        }
        */

    @Override
    public void onHandleIntent(Intent intent) {

        LogUtil.v(TAG, "service onHandleIntent: " + intent);
        LogUtil.d(TAG, "extras: " + (null == intent.getExtras() ? null : ""));
        if (null != intent.getExtras()) {

            for (String key : intent.getExtras().keySet()) {
                Object obj = intent.getExtras().get(key);
                LogUtil.v(TAG, "\t" + key + ": " + obj);
            }

            String activity = intent.getStringExtra("activity");

            Intent serviceIntent = new Intent();
            serviceIntent.setClassName("com.roamingaround.roamingarounddemo", "com.roamingaround.sdk.visits.service.RaEventService");


            List<DetectedActivity> detectedActivityList = new ArrayList<>();

            LogUtil.v(TAG, activity);

            if (activity.equals("moving")) {
                detectedActivityList.add((new DetectedActivity(DetectedActivity.STILL, 0)));
                detectedActivityList.add(new DetectedActivity(DetectedActivity.IN_VEHICLE, 80));
                detectedActivityList.add(new DetectedActivity(DetectedActivity.ON_BICYCLE, 5));
                detectedActivityList.add(new DetectedActivity(DetectedActivity.RUNNING, 15));
                detectedActivityList.add(new DetectedActivity(DetectedActivity.WALKING, 0));
            }
            else if (activity.equals("walking")) {
                detectedActivityList.add((new DetectedActivity(DetectedActivity.STILL, 10)));
                detectedActivityList.add(new DetectedActivity(DetectedActivity.IN_VEHICLE, 5));
                detectedActivityList.add(new DetectedActivity(DetectedActivity.ON_BICYCLE, 5));
                detectedActivityList.add(new DetectedActivity(DetectedActivity.RUNNING, 0));
                detectedActivityList.add(new DetectedActivity(DetectedActivity.WALKING, 80));
            }
            else if (activity.equals("still")) {
                detectedActivityList.add((new DetectedActivity(DetectedActivity.STILL, 80)));
                detectedActivityList.add(new DetectedActivity(DetectedActivity.IN_VEHICLE, 5));
                detectedActivityList.add(new DetectedActivity(DetectedActivity.ON_BICYCLE, 5));
                detectedActivityList.add(new DetectedActivity(DetectedActivity.RUNNING, 0));
                detectedActivityList.add(new DetectedActivity(DetectedActivity.WALKING, 10));
            }

            ActivityRecognitionResult aResult = new ActivityRecognitionResult(detectedActivityList, System.currentTimeMillis(), SystemClock.elapsedRealtime());
            serviceIntent.putExtra("com.google.android.location.internal.EXTRA_ACTIVITY_RESULT", aResult);
            startService(serviceIntent);
        }
    }
}