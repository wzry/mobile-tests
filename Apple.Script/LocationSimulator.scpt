on menu_click(mList)
	local appName, topMenu, r
            
	-- Validate our input
	if mList's length < 3 then error "Menu list is not long enough"
                    
	-- Set these variables for clarity and brevity later on
	set {appName, topMenu} to (items 1 through 2 of mList)
	set r to (items 3 through (mList's length) of mList)
                    
	-- This overly-long line calls the menu_recurse function with
	-- two arguments: r, and a reference to the top-level menu
	tell application "System Events" to my menu_click_recurse(r, ((process appName)'s ¬ 
	(menu bar 1)'s (menu bar item topMenu)'s (menu topMenu)))
end menu_click

on menu_click_recurse(mList, parentObject)
	local f, r

	-- `f` = first item, `r` = rest of items
	set f to item 1 of mList
	if mList's length > 1 then set r to (items 2 through (mList's length) of mList)
                    
	-- either actually click the menu item, or recurse again
	tell application "System Events"
		if mList's length is 1 then
			click parentObject's menu item f
		else
    		my menu_click_recurse(r, (parentObject's (menu item f)'s (menu f)))
		end if
	end tell
end menu_click_recurse
                    
On run arg
	set lat to item 1 of arg
	set lon to item 2 of arg                    
	application "Simulator" activate
	delay 0.2
	menu_click({"Simulator", "Debug", "Location", "None"})
	delay 0.2
	menu_click({"Simulator", "Debug", "Location", "Custom Location…"})
	delay 0.2
	tell application "System Events"
		tell process "Simulator"
        		set value of text field 1 of window "Custom Location" to lat -- Latitude
        		set value of text field 2 of window "Custom Location" to lon -- Longtitude
        		click UI Element "OK" of window "Custom Location"
    	end tell
	end tell
end run