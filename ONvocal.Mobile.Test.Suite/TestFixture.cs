﻿using System;
using ONvocal.Mobile.Test.Framework;
using Xunit;

namespace ONvocal.Mobile.Test.Suite
{
    public class AndroidLocationTestFixture : IDisposable
    {
        
        public AndroidLocationTestFixture()
        {
            //This will initialize the driver and launch the app
            Devices.SetAppiumDriver(Devices.Device.Android_Samusng_miniTab);

            //Open WIN CMD connection
            TestServers.OpenWinCmdConnection();
        }

        public void Dispose()
        {
            ConnectionClient.AndroidDriver.Quit();
            ConnectionClient.WinCmdConnection.Close();
        }

        [CollectionDefinition("Android location test collection")]
        public class AndroidLocationTestCollection : ICollectionFixture<AndroidLocationTestFixture>
        {
            // This class has no code, and is never created. Its purpose is simply
            // to be the place to apply [CollectionDefinition] and all the
            // ICollectionFixture<> interfaces.
        }
    }

    public class IosLocationTestFixture : IDisposable
    {
        public IosLocationTestFixture()
        {
            Devices.SetAppiumDriver(Devices.Device.IOS_Sim_Iphone6);
            TestServers.OpenIosSshConnection();
            //TODO: IOS profileID should save in configure file or find it from app
            GlobalVariables.IOSProfileId = "1538b5eb-ee97-4b17-b411-9bb102286a5d";
        }

        public void Dispose()
        {
            ConnectionClient.IosDriver.Quit();
            ConnectionClient.IosConnection.Disconnect();
            
        }

        [CollectionDefinition("Ios location test collection")]
        public class IosLocationTestCollection : ICollectionFixture<IosLocationTestFixture>
        {
            // This class has no code, and is never created. Its purpose is simply
            // to be the place to apply [CollectionDefinition] and all the
            // ICollectionFixture<> interfaces.
        }
    }
}
