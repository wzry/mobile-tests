﻿namespace ONvocal.Mobile.Test.Suite
{
    public static class Geofences
    {
        public enum Name
        {
            WaynesHouse100m,
            Newbury_240,
            Park1,
            Danvers,
        }
        public static string GetGeoRegionID(Name value)
        {
            switch (value)
            {
                case Name.WaynesHouse100m:
                    return "2D63F125-00E0-4B41-84B5-AEDD829C043C";

                case Name.Newbury_240:
                    return "7A262176-F5BD-4AFC-BDDD-8C2BFD0AD90A";

                case Name.Park1:
                    return "77B30F3D-13BD-44A4-8F35-EE0862B9CF59";

                case Name.Danvers:
                    return "3EC87385-9EF9-4382-BBB6-1EB62436114D";

                default:
                    return null;
            }
        }
    }
}
