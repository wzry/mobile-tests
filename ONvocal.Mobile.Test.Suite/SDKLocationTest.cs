﻿using System;
using Xunit;
using Xunit.Abstractions;
using ONvocal.Mobile.Test.Framework;
using System.Threading;


//TODO: Library below is for quick test of appmium library on the test project.
//TODO: Remove this library from project later.
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android; 
using OpenQA.Selenium.Appium.iOS;

using System.Diagnostics;
using Renci.SshNet;


namespace ONvocal.Mobile.Test.Suite
{
    [TestCaseOrderer("ONvocal.Mobile.Test.Suite.PriorityOrderer","ONvocal.Mobile.Test.Suite")]
    [Collection("Android location test collection")]
    public class AndroidLocationTest
    {
        [Theory, 
            InlineData(Devices.OS.Android),
            TestPriority(1)]
        public void WithNetwork_StartApp(Devices.OS OS) //start app at 240 Newbury location
        {
            //Start of the app is acutally called on the pretest fixture in AppiumDriverFixture class

            //Set location to 240 newbury street Office
            GlobalVariables.AndroidTimestamp1 = DateTime.Now;
            Devices.SetLocation(OS, Locations.GetLocation(Locations.Name.Newbury_240));

            Thread.Sleep(10000); //wait 10 second for the load icon to disappear. TODO, implement better wait function to handle this.

            //Get profileID 
           
            GlobalVariables.AndroidProfileId = Devices.GetProfileId(OS);
            Assert.NotNull(GlobalVariables.AndroidProfileId); //Verify profileID is not Null
            
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.Newbury_240), GlobalVariables.AndroidTimestamp1);
         

            //Background the app
            Devices.GoToHomeScreen(OS);
      
            //Should be only 1 enter event for this query in the DB
            Assert.Equal(1, Utilities.SQLDataRow());
            //Verify deviceID is not NULL.
            Assert.NotNull(Utilities.ReadSQLData(1, "DeviceId"));
            //Verify client timestamp is not null. 
            Assert.NotEqual("0001-01-01 00:00:00.0000000 +00:00", Convert.ToString(Utilities.ReadSQLData(1, "Timestamp")));
            //Verify clientEnterSubmissionTime is not null.
            Assert.NotNull(Utilities.ReadSQLData(1, "ClientEnterSubmissionTime"));
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(2)]
        public void WithNetwork_Geo1ToGeo2(Devices.OS OS) //Change location from Office to Park1
        {
            GlobalVariables.AndroidTimestamp2 = DateTime.Now; //Use timestamp2 beacuse still need to keep timestamp1 for later DB query.
            Devices.SetLocation(OS, Locations.GetLocation(Locations.Name.Park1));
            //Set device activity to moving
            Devices.SetAndroidActivity(Devices.activity.Moving);
            Devices.SetAndroidActivity(Devices.activity.Moving);

            //Change location
            Devices.SetLocation(OS, Locations.GetLocation(Locations.Name.Park1));

            //Set device activity to still
            Devices.SetAndroidActivity(Devices.activity.Still);
            Devices.SetAndroidActivity(Devices.activity.Still);
            Devices.WaitForGeoUpdate();
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(3)]
        public void WithNetwork_ExitGeo1InDB(Devices.OS OS) //Exit office geofence
        {

            //Query GeoExit table for Ra office exit event.

            DBQuery.QueryGeoRegionExitWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.Newbury_240), GlobalVariables.AndroidTimestamp2);
           
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
            //Verify deviceID is not NULL.
            Assert.NotNull(Utilities.ReadSQLData(1, "DeviceId"));
            //Verify client timestamp is not null. 
            Assert.NotEqual("0001-01-01 00:00:00.0000000 +00:00", Convert.ToString(Utilities.ReadSQLData(1, "Timestamp")));
            //Verify clientSubmissionTime is not null.
            Assert.NotNull(Utilities.ReadSQLData(1, "ClientSubmissionTime"));
        }

        [Fact, TestPriority(3)]
        public void ExitTimeUpdateInEnterTable() //Verifed exit time get updated in the GeoEntrance Table.
        {
            //Use timestamp 1 to Query GeoEntrance Table for Office enter event to see if exit time is updated
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.Newbury_240), GlobalVariables.AndroidTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
            //Check exit time is updated
            Assert.NotNull(Utilities.ReadSQLData(1, "ExitTimeStamp"));
            //Check ClientExitSubmissionTime is updated
            Assert.NotNull(Utilities.ReadSQLData(1, "ClientExitSubmissionTime"));
        }

        [Fact, TestPriority(3)]
        public void WithNetwork_EnterGeo2InDB() //Enter Park1
        {
            //Query GeoEntrance Table for Park1 enter event.
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.Park1), GlobalVariables.AndroidTimestamp2);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(4)]
        //From Park1 to Wayne's house with aireplane mode on at park1. 
        //The Jump in location contain region refresh for ios.
        public void AirPlaneMode_Geo1ToGeo2(Devices.OS OS) 
        {
            Devices.SetAirplaneModeOn(OS);
            GlobalVariables.AndroidTimestamp1 = DateTime.Now;
            Devices.SetAndroidActivity(Devices.activity.Moving);
            Devices.SetAndroidActivity(Devices.activity.Moving);
            Devices.SetLocation(OS, Locations.GetLocation(Locations.Name.WaynesHouse));
            Thread.Sleep(3000);
            Devices.SetAndroidActivity(Devices.activity.Still);
            Devices.SetAndroidActivity(Devices.activity.Still);
            Devices.TurnOnAllNetwork(OS);
            Devices.WaitForGeoUpdate();
        }

        [Fact, TestPriority(5)]
        public void AirPlaneMode_ExitGeo1InDB()
        {
            //Query GeoExit table for Park1 exit event.
            DBQuery.QueryGeoRegionExitWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.Park1), GlobalVariables.AndroidTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Fact, TestPriority(5)]
        public void AirPlaneMode_EnterGeo2InDB()
        {
            //Query GeoEnter table for wayne's house
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.WaynesHouse100m), GlobalVariables.AndroidTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(6)]
        //From Wayne's house to Danvers with out network. 
        //The Jump in location contain region refresh for ios.
        public void NoNetwork_Geo1ToGeo2(Devices.OS OS)
        {
            Devices.TurnOffAllNetwork(OS);
            GlobalVariables.AndroidTimestamp1 = DateTime.Now;
            Devices.SetAndroidActivity(Devices.activity.Moving);
            Devices.SetAndroidActivity(Devices.activity.Moving);
            Devices.SetLocation(OS, Locations.GetLocation(Locations.Name.Danvers));
            Devices.SetAndroidActivity(Devices.activity.Still);
            Devices.SetAndroidActivity(Devices.activity.Still);
            Devices.WaitForGeoUpdate();
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(7)]
        //From Danvers to office with out network. 
        //The Jump in location doesn't contain region refresh for ios.
        public void NoNetwork_Geo2ToGeo3(Devices.OS OS)
        {
            Devices.SetAndroidActivity(Devices.activity.Moving);
            Devices.SetAndroidActivity(Devices.activity.Moving);
            Devices.SetLocation(OS, Locations.GetLocation(Locations.Name.Newbury_240));
            Devices.SetAndroidActivity(Devices.activity.Still);
            Devices.SetAndroidActivity(Devices.activity.Still);
            Devices.WaitForGeoUpdate();
        }


        [Theory, InlineData(Devices.OS.Android), TestPriority(8)]
        //Turn On network at office and get SDK sent all cached location event to server.
        public void RegainNetworkAtGeo3(Devices.OS OS)
        {
            Devices.TurnOnAllNetwork(OS);
            Devices.WaitForGeoUpdate();
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(9)]
        public void RegainNetwork_Geo1ExitInDB(Devices.OS OS)
        {
            //Query GeoExit table for Wayne's house exit event.
            DBQuery.QueryGeoRegionExitWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.WaynesHouse100m), GlobalVariables.AndroidTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(9)]
        public void RegainNetwork_Geo2EnterInDB(Devices.OS OS)
        {
            //Query GeoEnter table for Danvers enter event.
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.Danvers), GlobalVariables.AndroidTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(9)]
        public void RegainNetwork_Geo2ExitInDB(Devices.OS OS)
        {
            //Query GeoExit table for Danvers exit event.
            DBQuery.QueryGeoRegionExitWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.Danvers), GlobalVariables.AndroidTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, 
        TestPriority(9),
        InlineData(Devices.OS.Android)]
        public void RegainNetwork_Geo3EnterInDB(Devices.OS OS)
        {
            //Query GeoEnter table for 240Nebury enter event.
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.Newbury_240), GlobalVariables.AndroidTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory,
        TestPriority(10),
        InlineData(Devices.OS.Android)]
        public void AppSwiped_Geo1ToGeo2(Devices.OS OS)
        {
            Devices.KillAppFromBackGround(OS);
            GlobalVariables.AndroidTimestamp1 = DateTime.Now;
            Devices.SetAndroidActivity(Devices.activity.Moving);
            Devices.SetAndroidActivity(Devices.activity.Moving);
            Devices.SetLocation(OS, Locations.GetLocation(Locations.Name.Danvers));
            Devices.SetAndroidActivity(Devices.activity.Still);
            Devices.SetAndroidActivity(Devices.activity.Still);
            Devices.WaitForGeoUpdate();
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(11)]
        public void AppSwiped_Geo1ExitInDB(Devices.OS OS)
        {
            //Query GeoExit table for 240 office exit event.
            DBQuery.QueryGeoRegionExitWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.Newbury_240), GlobalVariables.AndroidTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(11)]
        public void AppSwiped_Geo2EnterInDB(Devices.OS OS)
        {
            //Query GeoEnter table for Danvers enter event.
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.Danvers), GlobalVariables.AndroidTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory,
        TestPriority(12),
        InlineData(Devices.OS.Android)]
        public void PhoneRestart_Geo1ToGeo2(Devices.OS OS)
        {
            GlobalVariables.AndroidTimestamp1 = DateTime.Now;
            Devices.RebootDevice(Devices.OS.Android);
            Devices.SetAndroidActivity(Devices.activity.Still);
            Devices.SetAndroidActivity(Devices.activity.Still);
            Devices.WaitForGeoUpdate();
            //After the phone restart, it will take the actual location instead of Mock location, so it will back to the office
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(13)]
        public void PhoneRestarted_Geo2ExitInDB(Devices.OS OS)
        {
            //Query GeoExit table for Danver exit event.
            DBQuery.QueryGeoRegionExitWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.Danvers), GlobalVariables.AndroidTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(13)]
        public void PhoneRestarted_Geo1EnterInDB(Devices.OS OS)
        {
            //Query GeoEnter table for 240 office enter event.
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.AndroidProfileId, Geofences.GetGeoRegionID(Geofences.Name.Newbury_240), GlobalVariables.AndroidTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(14)]
        public void VisitLogInDB(Devices.OS OS)
        {
            DBQuery.QueryVisit(GlobalVariables.AndroidProfileId, GlobalVariables.AndroidTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); 
        }
    }

    //***********************************

    [TestCaseOrderer("ONvocal.Mobile.Test.Suite.PriorityOrderer", "ONvocal.Mobile.Test.Suite")]
    [Collection("Ios location test collection")]
    public class IOSLocationTest
    {
        [Theory,
            InlineData(Devices.OS.IOS),
            TestPriority(1)]
        public void WithNetwork_StartApp(Devices.OS OS) //start app at 240 Newbury location
        {
            //Start of the app is acutally called on the pretest fixture in AppiumDriverFixture class

            //Set location to 240 newbury street Office
            GlobalVariables.IOSTimestamp1 = DateTime.Now;
            Devices.SetLocation(OS, Locations.GetLocation(Locations.Name.Newbury_240));
            
            //Background the app
            Devices.GoToHomeScreen(OS);

            Devices.WaitForGeoUpdate();           
        }

        [Theory, InlineData(Devices.OS.IOS), TestPriority(2)]
        public void WithNetwork_Geo1ToGeo2(Devices.OS OS) //Change location from Office to Park1
        {
            GlobalVariables.IOSTimestamp2 = DateTime.Now; //Use timestamp2 beacuse still need to keep timestamp1 for later DB query.
            Devices.SetLocation(OS, Locations.GetLocation(Locations.Name.Park1));
            Devices.WaitForGeoUpdate();
        }

        [Theory, InlineData(Devices.OS.IOS), TestPriority(3)]
        public void WithNetwork_ExitGeo1InDB(Devices.OS OS) //Exit office geofence
        {

            //Query GeoExit table for Ra office exit event.
           
            DBQuery.QueryGeoRegionExitWithGeoID(GlobalVariables.IOSProfileId, Geofences.GetGeoRegionID(Geofences.Name.Newbury_240), GlobalVariables.IOSTimestamp2);
            
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
            //Verify deviceID is not NULL.
            Assert.NotNull(Utilities.ReadSQLData(1, "DeviceId"));
            //Verify client timestamp is not null. 
            Assert.NotEqual("0001-01-01 00:00:00.0000000 +00:00", Convert.ToString(Utilities.ReadSQLData(1, "Timestamp")));
            //Verify clientSubmissionTime is not null.
            Assert.NotNull(Utilities.ReadSQLData(1, "ClientSubmissionTime"));
        }

        [Fact, TestPriority(3)]
        public void ExitTimeUpdateInEnterTable() //Verifed exit time get updated in the GeoEntrance Table.
        {
            //Use timestamp 1 to Query GeoEntrance Table for Office enter event to see if exit time is updated
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.IOSProfileId, Geofences.GetGeoRegionID(Geofences.Name.Newbury_240), GlobalVariables.IOSTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
            //Check exit time is updated
            Assert.NotNull(Utilities.ReadSQLData(1, "ExitTimeStamp"));
            //Check ClientExitSubmissionTime is updated
            Assert.NotNull(Utilities.ReadSQLData(1, "ClientExitSubmissionTime"));
        }

        [Fact, TestPriority(3)]
        public void WithNetwork_EnterGeo2InDB() //Enter Park1
        {
            //Query GeoEntrance Table for Park1 enter event.
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.IOSProfileId, Geofences.GetGeoRegionID(Geofences.Name.Park1), GlobalVariables.IOSTimestamp2);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, InlineData(Devices.OS.IOS), TestPriority(4)]
        //From Park1 to Wayne's house with aireplane mode on at park1. 
        //The Jump in location contain region refresh for ios.
        public void AirPlaneMode_Geo1ToGeo2(Devices.OS OS)
        {
            Devices.SetAirplaneModeOn(OS);
            GlobalVariables.IOSTimestamp1 = DateTime.Now;
            Devices.SetLocation(OS, Locations.GetLocation(Locations.Name.WaynesHouse));
            Thread.Sleep(3000);
            Devices.TurnOnAllNetwork(OS);
            Devices.WaitForGeoUpdate();
        }

        [Fact, TestPriority(5)]
        public void AirPlaneMode_ExitGeo1InDB()
        {
            //Query GeoExit table for Park1 exit event.
            DBQuery.QueryGeoRegionExitWithGeoID(GlobalVariables.IOSProfileId, Geofences.GetGeoRegionID(Geofences.Name.Park1), GlobalVariables.IOSTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Fact, TestPriority(5)]
        public void AirPlaneMode_EnterGeo2InDB()
        {
            //Query GeoEnter table for wayne's house
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.IOSProfileId, Geofences.GetGeoRegionID(Geofences.Name.WaynesHouse100m), GlobalVariables.IOSTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, InlineData(Devices.OS.IOS), TestPriority(6)]
        //From Wayne's house to Danvers with out network. 
        //The Jump in location contain region refresh for ios.
        public void NoNetwork_Geo1ToGeo2(Devices.OS OS)
        {
            Devices.TurnOffAllNetwork(OS);
            GlobalVariables.IOSTimestamp1 = DateTime.Now;
            Devices.SetLocation(OS, Locations.GetLocation(Locations.Name.Danvers));
            Devices.WaitForGeoUpdate();
        }

        [Theory, InlineData(Devices.OS.IOS), TestPriority(7)]
        //From Danvers to office with out network. 
        //The Jump in location doesn't contain region refresh for ios.
        public void NoNetwork_Geo2ToGeo3(Devices.OS OS)
        {
            Devices.SetLocation(OS, Locations.GetLocation(Locations.Name.Newbury_240));
            Devices.WaitForGeoUpdate();
        }


        [Theory, InlineData(Devices.OS.IOS), TestPriority(8)]
        //Turn On network at office and get SDK sent all cached location event to server.
        public void RegainNetworkAtGeo3(Devices.OS OS)
        {
            Devices.TurnOnAllNetwork(OS);
            Devices.WaitForGeoUpdate();
        }

        [Theory, InlineData(Devices.OS.IOS), TestPriority(9)]
        public void RegainNetwork_Geo1ExitInDB(Devices.OS OS)
        {
            //Query GeoExit table for Wayne's house exit event.
            DBQuery.QueryGeoRegionExitWithGeoID(GlobalVariables.IOSProfileId, Geofences.GetGeoRegionID(Geofences.Name.WaynesHouse100m), GlobalVariables.IOSTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, InlineData(Devices.OS.IOS), TestPriority(9)]
        public void RegainNetwork_Geo2EnterInDB(Devices.OS OS)
        {
            //Query GeoEnter table for Danvers enter event.
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.IOSProfileId, Geofences.GetGeoRegionID(Geofences.Name.Danvers), GlobalVariables.IOSTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, InlineData(Devices.OS.IOS), TestPriority(9)]
        public void RegainNetwork_Geo2ExitInDB(Devices.OS OS)
        {
            //Query GeoExit table for Danvers exit event.
            DBQuery.QueryGeoRegionExitWithGeoID(GlobalVariables.IOSProfileId, Geofences.GetGeoRegionID(Geofences.Name.Danvers), GlobalVariables.IOSTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory,
        TestPriority(9),
        InlineData(Devices.OS.IOS)]
        public void RegainNetwork_Geo3EnterInDB(Devices.OS OS)
        {
            //Query GeoEnter table for 240Nebury enter event.
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.IOSProfileId, Geofences.GetGeoRegionID(Geofences.Name.Newbury_240), GlobalVariables.IOSTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory,
        TestPriority(10),
        InlineData(Devices.OS.IOS)]
        public void AppSwiped_Geo1ToGeo2(Devices.OS OS)
        {
            Devices.KillAppFromBackGround(OS);
            GlobalVariables.IOSTimestamp1 = DateTime.Now;
            Devices.SetLocation(OS, Locations.GetLocation(Locations.Name.Danvers));
            Devices.WaitForGeoUpdate();
        }

        [Theory, InlineData(Devices.OS.IOS), TestPriority(11)]
        public void AppSwiped_Geo1ExitInDB(Devices.OS OS)
        {
            //Query GeoExit table for 240 office exit event.
            DBQuery.QueryGeoRegionExitWithGeoID(GlobalVariables.IOSProfileId, Geofences.GetGeoRegionID(Geofences.Name.Newbury_240), GlobalVariables.IOSTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, InlineData(Devices.OS.IOS), TestPriority(11)]
        public void AppSwiped_Geo2EnterInDB(Devices.OS OS)
        {
            //Query GeoEnter table for Danvers enter event.
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.IOSProfileId, Geofences.GetGeoRegionID(Geofences.Name.Danvers), GlobalVariables.IOSTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory,
        TestPriority(12),
        InlineData(Devices.OS.IOS)]
        public void PhoneRestart_Geo1ToGeo2(Devices.OS OS)
        {
            GlobalVariables.IOSTimestamp1 = DateTime.Now;
            Devices.RebootDevice(Devices.OS.IOS);
            Devices.WaitForGeoUpdate();
            //After the phone restart, it will take the actual location instead of Mock location, so it will back to the office
        }

        [Theory, InlineData(Devices.OS.IOS), TestPriority(13)]
        public void PhoneRestarted_Geo2ExitInDB(Devices.OS OS)
        {
            //Query GeoExit table for Danver exit event.
            DBQuery.QueryGeoRegionExitWithGeoID(GlobalVariables.IOSProfileId, Geofences.GetGeoRegionID(Geofences.Name.Danvers), GlobalVariables.IOSTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }

        [Theory, InlineData(Devices.OS.IOS), TestPriority(13)]
        public void PhoneRestarted_Geo1EnterInDB(Devices.OS OS)
        {
            //Query GeoEnter table for 240 office enter event.
            DBQuery.QueryGeoRegionEntranceWithGeoID(GlobalVariables.IOSProfileId, Geofences.GetGeoRegionID(Geofences.Name.Newbury_240), GlobalVariables.IOSTimestamp1);
            Assert.Equal(1, Utilities.SQLDataRow()); //Should be only 1 enter event for this query in the DB
        }
    }


    /*

    //  [Collection("Ios location test collection")]
    [Collection("Android location test collection")]
    public class Test
    {
        private readonly ITestOutputHelper output;

        public Test(ITestOutputHelper o)
        {
            this.output = o;
        }

        [Theory, InlineData(Devices.OS.Android), TestPriority(0)]
        public void Test1(Devices.OS OS)
        {

            Devices.SetAndroidActivity(Devices.activity.Moving);

            ConnectionClient.WinCmdConnection.StandardInput.Close();
            output.WriteLine("This is output {0}", ConnectionClient.WinCmdConnection.StandardOutput.ReadToEnd());
            
        }
    }
    */
}
