using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("ONvocal.Mobile.Test.Suite")]
[assembly: AssemblyDescription("Test Project contain all mobile testing test cases. It using library created in ONvocal.Mobile.Test.Framework.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ONvocal")]
[assembly: AssemblyProduct("Mobile.Test.Suite")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("ef2900c9-9035-4827-940a-50337b235200")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
