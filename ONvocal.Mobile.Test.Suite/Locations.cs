﻿using OpenQA.Selenium.Appium;

namespace ONvocal.Mobile.Test.Suite
{
    public static class Locations
    {
        public enum Name
        {
            WaynesHouse,
            Newbury_240,
            Park1,
            Danvers,
        }

        public static Location GetLocation(Name value)
        {
            switch (value)
            {
                case Name.WaynesHouse:
                    var Waynes_house = new Location();
                    Waynes_house.Latitude = 42.2012225019211;
                    Waynes_house.Longitude = -70.9964219927133;
                    Waynes_house.Altitude = 120;
                    return Waynes_house;

                case Name.Newbury_240:
                    var RA_Office = new Location();
                    RA_Office.Latitude = 42.5826557387264;
                    RA_Office.Longitude = -70.9683129703626;
                    RA_Office.Altitude = 120;
                    return RA_Office;

                case Name.Park1:
                    var P1 = new Location();
                    P1.Latitude = 42.5864237640825;
                    P1.Longitude = -70.9818570611242;
                    P1.Altitude = 120;
                    return P1;

                case Name.Danvers:
                    var Dan = new Location();
                    Dan.Latitude = 42.5840982003012;
                    Dan.Longitude = -70.9476964471105;
                    Dan.Altitude = 120;
                    return Dan;

                default:
                    return null;
            }
        }
    }
}
