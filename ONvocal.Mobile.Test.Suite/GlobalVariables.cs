﻿using System;

namespace ONvocal.Mobile.Test.Suite
{
    public static class GlobalVariables
    {
        public static string AndroidProfileId { get; set; }
        public static string IOSProfileId { get; set; }
        public static DateTime AndroidTimestamp1 { get; set; }
        public static DateTime AndroidTimestamp2 { get; set; }
        public static DateTime IOSTimestamp1 { get; set; }
        public static DateTime IOSTimestamp2 { get; set; }
    }
}
