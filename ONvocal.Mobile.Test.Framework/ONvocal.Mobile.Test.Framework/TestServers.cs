﻿using System;
using Renci.SshNet;
using System.Diagnostics;

namespace ONvocal.Mobile.Test.Framework
{
    public static class TestServers
    {
        public static Uri Server1_Android { get { return new Uri("http://192.168.128.17:4723/wd/hub"); } }
        public static Uri Server2_IOS { get { return new Uri("http://192.168.128.34:4723/wd/hub"); } }

        public static void OpenIosSshConnection()
        {
            //SSH connection to Wayne's Mac book
            ConnectionClient.IosConnection = new SshClient("192.168.128.34", "drmichaeldavidarner", "22treeleaf");
            ConnectionClient.IosConnection.Connect();
        }
        public static void OpenWinCmdConnection()
        {
            //Connection to Wayne's WindowPC's cmd
            ConnectionClient.WinCmdConnection = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardInput = true;
            startInfo.RedirectStandardOutput = true;
            ConnectionClient.WinCmdConnection.StartInfo = startInfo;
            ConnectionClient.WinCmdConnection.Start();
        }
    }
}
