using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("ONvocal.Mobile.Test.Framework")]
[assembly: AssemblyDescription("Framework for mobile testing automation.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ONvocal")]
[assembly: AssemblyProduct("Mobile.Test.Framework")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("3455a616-1e7b-4d4c-9160-5203da75ca1d")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
