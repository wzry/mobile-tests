﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ONvocal.Mobile.Test.Framework
{
    public static class DBQuery
    {
        public static void QueryGeoRegionEntranceWithGeoID(string profileId, string GeoRegionId, DateTime EnterTime)
        {

            //Call method to Execute SQL for getting GeoRegionEntrance Table detail by ProfileId, GeoRegionId and Enter Time
            Utilities.PopulateSQLDataInCollection("SELECT * FROM GeoRegionEntrance WHERE profileId = '" + profileId + 
                "' and GeoRegionId = '" + GeoRegionId + 
                "' and ServerEnterSubmissionTime > '" + EnterTime +
                "' order by id desc");
        }

        public static void QueryGeoRegionEntrance(string profileId, DateTime EnterTime)
        {

            //Call method to Execute SQL for getting GeoRegionEntrance Table detail by ProfileId, GeoRegionId and Enter Time
            Utilities.PopulateSQLDataInCollection("SELECT * FROM GeoRegionEntrance WHERE profileId = '" + profileId +
                "' and ServerEnterSubmissionTime > '" + EnterTime +
                "' order by id desc");
        }

        public static void QueryGeoRegionExitWithGeoID(string profileId, string GeoRegionId, DateTime ExitTime)
        {

            //Call method to Execute SQL for getting GeoRegionExit Table detail by ProfileId, GeoRegionId and Exit Time
            Utilities.PopulateSQLDataInCollection("SELECT * FROM GeoRegionExit WHERE profileId = '" + profileId +
                "' and GeoRegionId = '" + GeoRegionId +
                "' and ServerSubmissionTime > '" + ExitTime +
                "' order by id desc");
        }

        public static void QueryGeoRegionExit(string profileId, DateTime ExitTime)
        {

            //Call method to Execute SQL for getting GeoRegionExit Table detail by ProfileId and Exit Time
            Utilities.PopulateSQLDataInCollection("SELECT * FROM GeoRegionExit WHERE profileId = '" + profileId +
                "' and ServerSubmissionTime > '" + ExitTime +
                "' order by id desc");
        }

        public static void QueryTempGeoEvent(string profileId, DateTime Timestamp)
        {

            //Call method to Execute SQL for getting TempGeoEvent Table detail by ProfileId, and Time Stamp
            Utilities.PopulateSQLDataInCollection("SELECT * FROM GeoRegionExit WHERE profileId = '" + profileId +
                "' and ServerSubmissionTime > '" + Timestamp +
                "' order by id desc");
        }

        public static void QueryVisit(string profileId, DateTime Timestamp)
        {

            //Call method to Execute SQL for getting Visit Table detail by ProfileId, and Time Stamp
            Utilities.PopulateSQLDataInCollection("SELECT * FROM Visit WHERE profileId = '" + profileId +
                "' and ServerSubmissionTime > '" + Timestamp +
                "' order by id desc");
        }     
    }
}
