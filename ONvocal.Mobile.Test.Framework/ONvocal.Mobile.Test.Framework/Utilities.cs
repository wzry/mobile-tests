﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;

namespace ONvocal.Mobile.Test.Framework
{
    public static class Utilities
    {
        //Method to get Data from SQL DB
        //Read data from excel to data table.
        public static DataTable SQLToDataTable(string SQLStatement)
        {
            //#region Building the connection string

            string Server = "rademo.database.windows.net";
            string Username = "rademo@rademo";
            string Password = "Demo12345";
            string Database = "rademo";
            string ConnectionString = "Data Source=" + Server + ";";
            ConnectionString += "User ID=" + Username + ";";
            ConnectionString += "Password=" + Password + ";";
            ConnectionString += "Initial Catalog=" + Database;
            //#endregion
            SqlConnection SQLConnection = new SqlConnection();
            try
            {
                SQLConnection.ConnectionString = ConnectionString;
                SQLConnection.Open();
                // You can get the server version
                //         Console.WriteLine("DB version: " + SQLConnection.ServerVersion);
            }
            catch (Exception Ex)
            {
                // Try to close the connection
                if (SQLConnection != null)
                    SQLConnection.Dispose();
                // Create a (useful) error message
                string ErrorMessage = "A error occurred while trying to connect to the server.";
                ErrorMessage += Environment.NewLine;
                ErrorMessage += Environment.NewLine;
                ErrorMessage += Ex.Message;
                // Show error message (this = the parent Form object)
                Console.WriteLine(ErrorMessage + "Connection error.");
                // Stop here
                return null;
            }

            //string SQLStatement = "Select * from campaignActivityScheduleDay where activityId = '090426b0-afc5-4151-abcb-551171680f8b'";
            // Create a SqlDataAdapter to get the results as DataTable
            SqlDataAdapter SQLDataAdapter = new SqlDataAdapter(SQLStatement, SQLConnection);
            // Create a new DataTable
            DataTable dtResult = new DataTable();
            // Fill the DataTable with the result of the SQL statement
            SQLDataAdapter.Fill(dtResult);
            /*        // Loop through all entries
                    foreach (DataRow drRow in dtResult.Rows)
                    {
                        foreach (DataColumn drCol in dtResult.Columns)
                        {
                            Console.WriteLine(drCol.ColumnName.ToString());
                            // Show a message box with the content of
                            // the "Name" column
                            Console.WriteLine(drRow[drCol].ToString());
                        }
                    }
             */
            // We don't need the data adapter any more
            SQLDataAdapter.Dispose();
            SQLConnection.Close();
            SQLConnection.Dispose();
            return dtResult;
        }

        //Populating Data into list collections
        static List<SQLDataCollection> sqlDataCol = new List<SQLDataCollection>();

        static DataTable table;
        public static void PopulateSQLDataInCollection(string SQLStatement)
        {
            table = SQLToDataTable(SQLStatement);

            //Iterate through the rows and columns of the Table
            for (int row = 1; row <= table.Rows.Count; row++)
            {
                for (int col = 0; col < table.Columns.Count; col++)
                {
                    SQLDataCollection sqlTable = new SQLDataCollection()
                    {
                        sqlRowNumber = row,
                        sqlColName = table.Columns[col].ColumnName,
                        sqlColValue = table.Rows[row - 1][col].ToString()
                    };
                    //Add all the details for each row
                    sqlDataCol.Add(sqlTable);
                }
            }
        }

        //Reading data from Collection
        public static string ReadSQLData(int sqlRowNumber, string sqlColumnName)
        {
            try
            {
                //Retriving Data using LINQ to reduce much of iterations
                string data = (from colData in sqlDataCol
                               where colData.sqlColName == sqlColumnName && colData.sqlRowNumber == sqlRowNumber
                               select colData.sqlColValue).LastOrDefault();

                //var datas = dataCol.Where(x => x.colName == columnName && x.rowNumber == rowNumber).SingleOrDefault().colValue;
                return data.ToString();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static int SQLDataRow()
        {
            return table.Rows.Count;
        }
   

        public class SQLDataCollection
        {
            public int sqlRowNumber { get; set; }
            public string sqlColName { get; set; }
            public string sqlColValue { get; set; }
        }
    }
}
