﻿using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.iOS;
using Renci.SshNet;
using System.Diagnostics;

namespace ONvocal.Mobile.Test.Framework
{
    public static class ConnectionClient
    {
        public static AndroidDriver<AndroidElement> AndroidDriver { get; set; }
        public static IOSDriver<IOSElement> IosDriver { get; set; }
        public static SshClient IosConnection { get; set; }
        public static Process WinCmdConnection{ get; set; }
    }
}
