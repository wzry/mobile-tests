﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.iOS;
using System.Threading;

namespace ONvocal.Mobile.Test.Framework
{
    public static class Devices
    {
        private static DesiredCapabilities capabilities = new DesiredCapabilities();
        public enum Device
        {
            IOS_Sim_Iphone6,
            Android_Samsung_s7,
            Android_Samusng_miniTab,
        }

        public enum OS
        {
            IOS,
            Android,
        }

        public enum activity
        {
            Still,
            Moving,
            Walking,
        }

        public static void SetAppiumDriver(Device value)
        {
            switch (value)
            {
                case Device.IOS_Sim_Iphone6:

                    capabilities.SetCapability("platformName", "iOS");
                    capabilities.SetCapability("platformVersion", "10.2");
                    capabilities.SetCapability("deviceName", "iPhone");
                    capabilities.SetCapability("Simulated Model", "iPhone 6");
                    capabilities.SetCapability("udid", "F86D35A9-B150-4C1D-A7F8-5985D516CC50");
                    capabilities.SetCapability("bundleId", "com.roamingaround.waynetest.testapp2");
                    // capabilities.SetCapability("app", "/Users/ADMIN/Desktop/CAMobileApp.app");  
                   // capabilities.SetCapability("noReset", "true");
                    ConnectionClient.IosDriver = new IOSDriver<IOSElement>(TestServers.Server2_IOS, capabilities, TimeSpan.FromSeconds(60));
                    ConnectionClient.IosDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
                    
                    break;
                      
                case Device.Android_Samsung_s7:

                    //Android Samsung S7 on Wayne's Window PC
                    capabilities.SetCapability("deviceName", "d46acb17");
                    capabilities.SetCapability("platformName", "Android");
                    capabilities.SetCapability("platformVersion", "7.0");                
                    capabilities.SetCapability("appPackage", "com.roamingaround.rapidapp");
                    capabilities.SetCapability("appActivity", "com.roamingaround.rapidapp.ui.MainActivity");
                    ConnectionClient.AndroidDriver = new AndroidDriver<AndroidElement>(TestServers.Server1_Android, capabilities, TimeSpan.FromSeconds(60));
                    ConnectionClient.AndroidDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
                    break;

                case Device.Android_Samusng_miniTab:
                          
                    //Android Samsung mini Tablet on Wayne's Window PC      
                    capabilities.SetCapability("deviceName", "31005c3fa0cab200");
                    capabilities.SetCapability("platformName", "Android");
                    capabilities.SetCapability("platformVersion", "5.1.1");
                    capabilities.SetCapability("appPackage", "com.roamingaround.roamingarounddemo");
                    capabilities.SetCapability("appActivity", "com.roamingaround.sdk.app.MainActivity");
                    capabilities.SetCapability("newCommandTimeout", "False");
                //  capabilities.SetCapability("noReset", "true");
                //  capabilities.SetCapability("dontStopAppOnReset", "true");          
                    ConnectionClient.AndroidDriver = new AndroidDriver<AndroidElement>(TestServers.Server1_Android, capabilities, TimeSpan.FromSeconds(60));
                    ConnectionClient.AndroidDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);                    
                    break;
            }
        }

        public static void SetLocation(OS value, Location loc)
        {
            switch (value)
            {
                case OS.Android:
                    ConnectionClient.AndroidDriver.Location = loc;
                    break;

                case OS.IOS:
                    ConnectionClient.IosConnection.RunCommand("osascript Desktop/LocationSimulator.scpt '" + loc.Latitude + "' '" + loc.Longitude + "'");
                    break;
            }
        }

        public static void SetAndroidActivity(activity value)
        {
            ConnectionClient.WinCmdConnection.StandardInput.WriteLine(
                "adb shell am startservice -n \"com.roamingaround.roamingarounddemo/com.roamingaround.sdk.app.ActivityMockTestService\" --es activity " + value.ToString().ToLower());
        }

        public static void OpenSettingMenu(OS value)
        {
            switch (value)
            {
                case OS.IOS:
                    GoToHomeScreen(value);
                    Thread.Sleep(2000);
                    //swipe up the settings menu from home screen.
                    int swipe_startX = (int)150;
                    int swipe_startY = (int)667;
                    int swipe_endx = (int)151;
                    int swipe_endy = (int)200;
                    int swipe_duration = (int)1;
                    ConnectionClient.IosDriver.Swipe(swipe_startX, swipe_startY, swipe_endx, swipe_endy, swipe_duration);
                    break;
            }
        }

        public static void SetAirplaneModeOn(OS value)
        {
            switch (value)
            {
                case OS.Android:
                   
                    ConnectionClient.AndroidDriver.ConnectionType = ConnectionType.AirplaneMode;
                    break;
            
                case OS.IOS:

                    OpenSettingMenu(value);

                    string status = ConnectionClient.IosDriver.FindElementByAccessibilityId("Airplane Mode").GetAttribute("value");

                    //Check if airplane mode is off or not, if Not on, tap the icon
                    if (status == "false")
                    {
                        ConnectionClient.IosDriver.FindElementByAccessibilityId("Airplane Mode").Tap(1, 1);
                    }
                    //this is to close the seting menu by clicking outside of it.
                    ConnectionClient.IosDriver.Tap(1, 150, 150, 1); 
                    break;
            }
        }


        public static void SetAirplaneModeOff(OS value)
        {
            switch (value)
            {
                //On Android, setting airplaneMode off is same as changing the connection to all network On.
                case OS.Android:
                    ConnectionClient.AndroidDriver.ConnectionType = ConnectionType.AllNetworkOn;
                    break;

                case OS.IOS:
                    OpenSettingMenu(value);
                    string status = ConnectionClient.IosDriver.FindElementByAccessibilityId("Airplane Mode").GetAttribute("value");

                    //Check if airplane mode is on or not. If On, tap the icon to turn it back on.
                    if (status == "true")
                    {
                        ConnectionClient.IosDriver.FindElementByAccessibilityId("Airplane Mode").Tap(1, 1);
                    }
                    //this is to close the seting menu by clicking outside of it.
                    ConnectionClient.IosDriver.Tap(1, 150, 150, 1);
                    break;
            }
        }

        public static void SetWifiOn(OS value)
        {
            switch (value)
            {
                case OS.Android:
                    ConnectionClient.AndroidDriver.ConnectionType = ConnectionType.WifiOnly;
                    break;

                case OS.IOS:
                    OpenSettingMenu(value);
                    string status = ConnectionClient.IosDriver.FindElementByAccessibilityId("Wi-Fi").GetAttribute("value");

                    //Check if wifi is on or not. If off, tap the icon to turn it back on.
                    if (status == "false")
                    {
                        ConnectionClient.IosDriver.FindElementByAccessibilityId("Wi-Fi").Tap(1, 1);
                    }
                    //this is to close the seting menu by clicking outside of it.
                    ConnectionClient.IosDriver.Tap(1, 150, 150, 1);
                    break;
            }
        }

        public static void SetIOSWifiOff(OS value)
        {
            switch (value)
            {
                //This is for ios Only, for android, you can set dataOnly or set all network off.
                case OS.IOS:
                    OpenSettingMenu(value);
                    string status = ConnectionClient.IosDriver.FindElementByAccessibilityId("Wi-Fi").GetAttribute("value");

                    //Check if wifi is on or not. If On, tap the icon to turn it off.
                    if (status == "true")
                    {
                        ConnectionClient.IosDriver.FindElementByAccessibilityId("Wi-Fi").Tap(1, 1);
                    }
                    //this is to close the seting menu by clicking outside of it.
                    ConnectionClient.IosDriver.Tap(1, 150, 150, 1);
                    break;
            }
        }

        public static void SetDataOn(OS value)
        {
            //This is only on Android now, since we do IOS on simulator only, which doesn't have dataplan
            switch (value)
            {
                case OS.Android:
                    ConnectionClient.AndroidDriver.ConnectionType = ConnectionType.DataOnly;
                    break;
            }
        }

        public static void TurnOffAllNetwork(OS value)
        {
            switch (value)
            {
                case OS.Android:
                    ConnectionClient.AndroidDriver.ConnectionType = ConnectionType.None;
                    break;
            }
        }


        public static void TurnOnAllNetwork(OS value)
        {
            switch (value)
            {
                case OS.Android:
                    ConnectionClient.AndroidDriver.ConnectionType = ConnectionType.AllNetworkOn;
                    break;
            }
        }

        public static void WaitForGeoUpdate()
        {
            Thread.Sleep(240000); //set to wait for 4 mintues.
        }

        public static void GoToHomeScreen(OS value)
        {
            switch (value)
            {
                case OS.Android:
                    ConnectionClient.AndroidDriver.PressKeyCode(AndroidKeyCode.Home);
                    break;
                case OS.IOS:
                    //Run apple script on the Mac to simulator going to home screen.
                    ConnectionClient.IosConnection.RunCommand("osascript Desktop/LaunchHomeScreen.scpt");
                    break;
            }
        }

        //TODO: Put appName as parameter, and configure it on the App class.
        public static void KillAppFromBackGround(OS value)
        {
            switch (value)
            {
                case OS.Android:
                    ConnectionClient.AndroidDriver.PressKeyCode(AndroidKeyCode.Keycode_APP_SWITCH);
                    ConnectionClient.AndroidDriver.FindElementByAccessibilityId("RA SDK").FindElementById("com.android.systemui:id/dismiss_task").Click();
                    break;

                case OS.IOS:
                    GoToHomeScreen(value);
                    Thread.Sleep(1000);
                    //Run the apple script to simulate a double tap home button on simulator.
                    ConnectionClient.IosConnection.RunCommand("osascript Desktop/OpenIOSAppSwitch.scpt");
                    Thread.Sleep(1000);
                    //swipe up the app from app switch screen to kill it.
                    int swipe_startX = (int)185;
                    int swipe_startY = (int)300;
                    int swipe_endx = (int)186;
                    int swipe_endy = (int)0;
                    int swipe_duration = (int)1;
                    ConnectionClient.IosDriver.Swipe(swipe_startX, swipe_startY, swipe_endx, swipe_endy, swipe_duration);
                    GoToHomeScreen(value);
                    break;
            }
        }

        public static void RebootDevice(OS value)
        {
            switch (value)
            {
                case OS.Android:
                    ConnectionClient.WinCmdConnection.StandardInput.WriteLine("adb reboot");
                    break;
                case OS.IOS:
                    //Run apple script on the Mac to simulator device reboot on the simulator menu.
                    ConnectionClient.IosConnection.RunCommand("osascript Desktop/RebootIOSSim.scpt");
                    break;
            }
        }

        //TODO: This method need to change to under app class later.
        //And as we have more app in the framework, it can further refactor under specific appname.pagename object
        public static string GetProfileId(OS value)
        {
            switch (value)
            {
                case OS.Android:
                    string profileID = ConnectionClient.AndroidDriver.FindElementsByClassName("android.widget.TextView")[2].Text;
                    return profileID;

                default:
                    return null;
            }
        }


    }
}
